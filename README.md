# Portobello - Full-Stack Web Developer #

Projeto para avaliação de conhecimentos
The applicant must develop the responsive front-end of a news website according to the mockups and specs provided. The news content should come from a back-end (also developed by the applicant) REST API.

### MVP ###

1. Create a private repository on Bitbucket and add @USUARIO_DA_PORTOBELLO as admin
2. Requirements:
* All development should be done in a secondary (non-master) branch
* The layout should precisely match the provided mockups
* The website should be front-end rendered, using ReactJS
* The news content should be provided by a REST API, developed in Python using Django + Django REST Framework (the content can be manually input in the database)
* Both front- and back-end should be deployed to an online environment and the URL should be sent to EMAIL_DA_PORTOBELLO
3. A pull request to the master branch containing all the code should be created and assigned to @USUARIO_DA_PORTOBELLO when the challenge is completed.

### Extras ###

1. CSS animations
2. Loading transitions
3. Usage of containers
4. Dynamically loaded news: a background scraping task should periodically retrieve news from an online feed, such as TechCrunch
5. Integration with CI tools